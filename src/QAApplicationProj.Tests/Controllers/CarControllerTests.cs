﻿using QAApplicationProj.Controllers;
using QAApplicationProj.Data.Repositories;
using QAApplicationProj.Requests;
using QAApplicationProj.Responses;
using System;
using Xunit;

namespace QAApplicationProj.Tests.Controllers
{
    public class CarControllerTests {
        private readonly CarController _controller;
        public CarControllerTests() {
            /* TODO; Hint: figure out a solution to avoid communicating with the database, then change the null reference to an instance of ICarRepository. 
             * You can choose to use a 3rd party library in NuGet if you'd like. */
            _controller = new CarController(new CarRepository()); 
        }
       
        [Fact]
        public void CreateCar_Null()
        {
            CreateCarResponse response = _controller.CreateCar(null);
            Assert.False(response.WasSuccessful);
            Assert.Equal(response.Message, "The request did not pass validation.");
        }

        [Fact]
        public void CreateCar_InvalidRequest()
        {
            CarRequest request = new CarRequest();
            CreateCarResponse response=  _controller.CreateCar(request);
            Assert.False(response.WasSuccessful);
            Assert.Equal(response.Message, "The request did not pass validation.");            
        }

        [Fact]
        public void CreateCar_InvalidRequest_IncompleteModelYear()
        {
            CarRequest request = new CarRequest();
            request.Make = "Honda";
            CreateCarResponse response = _controller.CreateCar(request);
            Assert.False(response.WasSuccessful);
            Assert.Equal(response.Message, "The request did not pass validation.");
        }

        [Fact]
        public void CreateCar_InvalidRequest_IncompleteYear()
        {
            CarRequest request = new CarRequest();
            request.Make = "Honda";
            request.Model = "Civic";
            CreateCarResponse response = _controller.CreateCar(request);
            Assert.False(response.WasSuccessful);
            Assert.Equal(response.Message, "The request did not pass validation.");
        }


        [Fact]
        public void CreateCar_ValidRequest_ExistingCar()
        {
            CarRequest request = new CarRequest();
            request.Make = "Honda";
            request.Model = "Civic";
            request.Year = 2010;
            CreateCarResponse response = _controller.CreateCar(request);
            Assert.False(response.WasSuccessful);
            Assert.Equal(response.Message, "The requested Car already exists.");
        }

        [Fact]
        public void CreateCar_ValidRequest_NewCar()
        {
            CarRequest request = new CarRequest();
            request.Make = "Tesla";
            request.Model = "S";
            request.Year = 2017;
            CreateCarResponse response = _controller.CreateCar(request);
            Assert.True(response.WasSuccessful);
            Assert.Null(response.Message);
        }
    }
}
