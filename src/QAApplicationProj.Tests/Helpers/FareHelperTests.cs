﻿using QAApplicationProj.Helpers;
using Xunit;
using System;

namespace QAApplicationProj.Tests.Helpers
{
    public class FareHelperTests
    {
        // TODO: Add your test method(s) here.

        // argument type Decimal has the following test values of interest {null,negative,0,positive,MAXVALUE}

        [Fact]
        public void AddFares_NullZero()
        {
            try
            {
                FareHelper.AddFares(new decimal(null), Decimal.Zero);
            }
            catch (System.ArgumentNullException e)
            {
                // expected
            }
            catch (System.Exception e)
            {
                Assert.True(false, "Expected System.ArgumentNullException to be thrown on first argument");
            }
        }
        [Fact]
        public void AddFares_ZeroNull() 
        {
            try
            {
                FareHelper.AddFares(Decimal.Zero, new decimal(null));
            }
            catch (System.ArgumentNullException e)
            {
                // expected
            }
            catch (System.Exception e)
            {
                Assert.True(false, "Expected System.ArgumentNullException to be thrown on second argument");
            }
        }
        [Fact]
        public void AddFares_NegativeAny()
        {
            try
            {
                FareHelper.AddFares(Decimal.MinusOne, Decimal.Zero);
                //
            }
            catch (System.ArgumentOutOfRangeException e)
            {
                // expected
            }
            catch (System.Exception e)
            {
                Assert.True(false, "Expected System.ArgumentOutOfRangeException to be thrown on first argument");
            }
        }
        [Fact]
        public void AddFares_ZeroNegative()
        {
            try
            {
                FareHelper.AddFares(Decimal.Zero, Decimal.MinusOne);
                Assert.True(false, "No exception thrown. Expected System.ArgumentOutOfRangeException");
            }
            catch (System.ArgumentOutOfRangeException e)
            {
                // expected
            }
            catch (System.Exception e)
            {
                Assert.True(false, "Expected System.ArgumentOutOfRangeException to be thrown on second argument");
            }
        }

        [Fact]
        public void AddFares_ZeroZero()
        {
                Decimal  result = FareHelper.AddFares(Decimal.Zero, Decimal.Zero);
                Assert.Equal(result,decimal.Zero);
        }
        [Fact]
        public void AddFares_ZeroOne()
        {
            Decimal result = FareHelper.AddFares(Decimal.Zero, Decimal.One);
            Assert.Equal(result, decimal.One);
        }
        [Fact]
        public void AddFares_ZeroMaxValue()
        {
            Decimal result = FareHelper.AddFares(Decimal.Zero, Decimal.MaxValue);
            Assert.Equal(result, decimal.MaxValue);
        }
        [Fact]
        public void AddFares_OneNull()
        {
            // A bit redundant since a test for non null on first argument exists with ZeroNull
            try
            {
                FareHelper.AddFares(Decimal.One, new decimal(null));
            }
            catch (System.ArgumentNullException e)
            {
                // expected
            }
            catch (System.Exception e)
            {
                Assert.True(false, "Expected System.ArgumentNullException to be thrown on second argument");
            }
        }
        [Fact]
        public void AddFares_OneZero()
        {
            Decimal result = FareHelper.AddFares(Decimal.One,Decimal.Zero);
            Assert.Equal(result, decimal.One);
        }

        [Fact]
        public void AddFares_OneOne()
        {
            Decimal result = FareHelper.AddFares(Decimal.One, Decimal.One);
            Assert.Equal(result,new decimal(2));
        }


        [Fact]
        public void AddFares_OneMaxValue()
        {
            try
            {
                Decimal result = Decimal.One;
                result = FareHelper.AddFares(Decimal.One, Decimal.MaxValue);
                Assert.True(false, "No exception thrown. Expected System.OverflowException to be thrown");
            }
            catch (System.OverflowException e)
            {
                // expected
            }
            catch (System.Exception e)
            {
                Assert.True(false,"Expected System.OverflowException to be thrown");
            }
        }
    }
}
